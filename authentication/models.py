from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    """Tunable user class"""

    def __str__(self):
        if self.first_name == "":
            return f"{self.username}"
        else:
            return f"{self.first_name} {self.last_name}"

