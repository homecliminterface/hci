from rest_framework.serializers import ModelSerializer, StringRelatedField, SerializerMethodField

from .models import HeaterValve, TemperatureSchedule, DefaultTemperature


class HeaterValveSerializer(ModelSerializer):

    schedule_name = SerializerMethodField('get_schedule_name')

    def get_schedule_name(self, valve):
        if valve.schedule:
            return valve.schedule.name
        return ""

    class Meta:
        model = HeaterValve
        read_only_fields = [
            'id',
            'manufacturer',
            'model',
            'specifications',
            'friendly_name',
            'ieee_address',
            'heat_required',
            'measured_temperature',
            'wanted_temperature',
            'schedule',
            'schedule_name',
        ]
        fields = [
            'id',
            'manufacturer',
            'model',
            'specifications',
            'friendly_name',
            'ieee_address',
            'heat_required',
            'measured_temperature',
            'wanted_temperature',
            'schedule',
            'schedule_name',
        ]


class HeaterValveSetTemperatureSerializer(ModelSerializer):

    class Meta:
        model = HeaterValve
        fields = ['wanted_temperature', ]


class HeaterValveSetScheduleSerializer(ModelSerializer):

    class Meta:
        model = HeaterValve
        fields = ['schedule', ]


class TemperatureScheduleSerializer(ModelSerializer):

    class Meta:
        model = TemperatureSchedule
        read_only_fields = ['id', 'name']
        fields = ['id', 'name']


class TemperatureScheduleDetailSerializer(ModelSerializer):

    class Meta:
        model = TemperatureSchedule
        fields = ['id', 'name', 'schedule']


class TemperatureScheduleUpdateScheduleSerializer(ModelSerializer):
    class Meta:
        model = TemperatureSchedule
        fields = ['schedule']


class TemperatureScheduleUpdateNameSerializer(ModelSerializer):
    class Meta:
        model = TemperatureSchedule
        fields = ['name']


class DefaultTemperatureSerializer(ModelSerializer):

    class Meta:
        model = DefaultTemperature
        fields = '__all__'


class DefaultTemperatureUpdateTemperatureSerializer(ModelSerializer):

    class Meta:
        model = DefaultTemperature
        fields = ['temperature']
