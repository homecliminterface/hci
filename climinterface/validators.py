from django.utils.translation import gettext_lazy as _
from rest_framework.serializers import ValidationError as SerializerValidationError


class TemperatureValidator:
    """Class for wanted temperature validator.

    Parameters depend on the instance (minimum and maximum temperature allowed, step for selecting temperature).
    """

    def __init__(self, precision, min_value, max_value):
        self.precision = precision
        self.min_value = min_value
        self.max_value = max_value

    def __call__(self, value):
        self.validate_interval(value)
        self.validate_precision(value)

    def validate_precision(self, value):
        if (value * 10) % (self.precision * 10) != 0:
            raise SerializerValidationError(
                _('maximum precision is %(precision), but the value %(value) was given'),
                params={'precision': self.precision, 'value': value}
            )

    def validate_interval(self, value):
        if value < self.min_value or value > self.max_value:
            raise SerializerValidationError(
                _('The temperature has to be set between %(min) and %(max), but the value %(value) was given'),
                params={'min': self.min_value, 'max': self.max_value, 'value': value}
            )

