from django.apps import AppConfig

import threading
import time
import schedule


class CliminterfaceConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'climinterface'





    def ready(self):

        from climinterface.models import MessageProcessing
        import schedule
        from climinterface.models import HeaterValve

        def run_continuously(interval=1):
            """Continuously run, while executing pending jobs at each
            elapsed time interval.
            @return cease_continuous_run: threading. Event which can
            be set to cease continuous run. Please note that it is
            *intended behavior that run_continuously() does not run
            missed jobs*. For example, if you've registered a job that
            should run every minute, and you set a continuous run
            interval of one hour, then your job won't be run 60 times
            at each interval but only once.
            """
            cease_continuous_run = threading.Event()

            class ScheduleThread(threading.Thread):
                @classmethod
                def run(cls):
                    while not cease_continuous_run.is_set():
                        schedule.run_pending()
                        time.sleep(interval)

            continuous_thread = ScheduleThread()
            continuous_thread.start()
            return cease_continuous_run

        MessageProcessing.create_and_start_paho_mqtt_client()

        schedule.every().minute.at(":05").do(HeaterValve.set_temperatures_from_schedules)
        run_continuously(15)


