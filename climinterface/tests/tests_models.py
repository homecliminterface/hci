from django.test import TestCase


from ..models import HeaterValve, ValveSpecifications, TemperatureSchedule
from ..helpers import generate_schedule_dict


class TestHeaterValve(TestCase):
    """Test creation of HeaterValve objects and `set_specifications()` method.

    The `set_specifications()` method is tested when a ValveSpecifications object matches, and when there is no match.
    """

    def test_specifications(self):

        schedule1 = TemperatureSchedule.objects.create(
            name="Test schedule",
            schedule=generate_schedule_dict()
        )

        specification1 = ValveSpecifications.objects.create(
            manufacturer='Danfoss',
            model='014G2461',
            temperature_min=5,
            temperature_max=35,
            step=0.5
        )

        valve1 = HeaterValve.objects.create(
            manufacturer="Danfoss",
            model="014G2461",
            specifications=specification1,
            friendly_name="vanne_1",
            ieee_address="0xefzare233",
            heat_required=False,
            measured_temperature=23,
            wanted_temperature=30,
            schedule=TemperatureSchedule.objects.get(pk=schedule1.id),
        )

        valve2 = HeaterValve.objects.create(
            manufacturer="Danfoss",
            model="014G2462",
            friendly_name="vanne_1",
            ieee_address="0xefzare2433",
            heat_required=False,
            measured_temperature=23,
            wanted_temperature=30,
            schedule=TemperatureSchedule.objects.get(pk=schedule1.id),
        )

        valve1.set_specifications()
        valve2.set_specifications()

        self.assertEqual(valve1.specifications, specification1)
        self.assertEqual(valve2.specifications, None)



