import django.db.models
from rest_framework.test import APITestCase
from django.urls import reverse_lazy

from authentication.models import User

from ..models import HeaterValve, ValveSpecifications, TemperatureSchedule, DefaultTemperature
from ..helpers import generate_schedule_dict

def authenticate(client):
    user = User.objects.create_user("testinguser")
    client.force_authenticate(user=user)

class TestHeaterValve(APITestCase):
    """Test api endpoints for heater valves"""

    url = reverse_lazy('heatervalves-list')

    def test_list(self):

        authenticate(self.client)

        schedule1 = TemperatureSchedule.objects.create(
            name="Test schedule",
            schedule=generate_schedule_dict()
        )

        specification1 = ValveSpecifications.objects.create(
            manufacturer='Danfoss',
            model='014G2461',
            temperature_min=5,
            temperature_max=35,
            step=0.5
        )

        valve1 = HeaterValve.objects.create(
            manufacturer="Danfoss",
            model="014G2461",
            specifications=specification1,
            friendly_name="vanne_1",
            ieee_address="0xefzare233",
            heat_required=False,
            measured_temperature=23,
            wanted_temperature=30,
            schedule=schedule1,
        )

        valve2 = HeaterValve.objects.create(
            manufacturer="Danfoss",
            model="014G2462",
            friendly_name="vanne_1",
            ieee_address="0xefzare2333",
            heat_required=False,
            measured_temperature=23,
            wanted_temperature=30,
            schedule=schedule1,
        )

        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)

        expected = [
            {
                'id': valve1.pk,
                'manufacturer': valve1.manufacturer,
                'model': valve1.model,
                'specifications': 1,
                'friendly_name': valve1.friendly_name,
                'ieee_address': valve1.ieee_address,
                'heat_required': valve1.heat_required,
                'measured_temperature': valve1.measured_temperature,
                'wanted_temperature': valve1.wanted_temperature,
                'schedule': schedule1.id,
                'schedule_name': schedule1.name,
            },
            {
                'id': valve2.pk,
                'manufacturer': valve2.manufacturer,
                'model': valve2.model,
                'specifications': valve2.specifications,
                'friendly_name': valve2.friendly_name,
                'ieee_address': valve2.ieee_address,
                'heat_required': valve2.heat_required,
                'measured_temperature': valve2.measured_temperature,
                'wanted_temperature': valve2.wanted_temperature,
                'schedule': schedule1.id,
                'schedule_name': schedule1.name
            }
        ]
        self.assertEqual(expected, response.json())

    def test_detail(self):

        authenticate(self.client)

        schedule1 = TemperatureSchedule.objects.create(
            name="Test schedule",
            schedule=generate_schedule_dict()
        )

        specification1 = ValveSpecifications.objects.create(
            manufacturer='Danfoss',
            model='014G2461',
            temperature_min=5,
            temperature_max=35,
            step=0.5
        )

        valve1 = HeaterValve.objects.create(
            manufacturer="Danfoss",
            model="014G2461",
            specifications=specification1,
            friendly_name="vanne_1",
            ieee_address="0xefzare233",
            heat_required=False,
            measured_temperature=23,
            wanted_temperature=30,
            schedule=TemperatureSchedule.objects.get(pk=schedule1.id)
        )

        url_valve_1 = self.url + str(valve1.pk) + "/"
        print(url_valve_1)
        response = self.client.get(url_valve_1)

        self.assertEqual(response.status_code, 200)

        expected = {
                'id': valve1.pk,
                'manufacturer': valve1.manufacturer,
                'model': valve1.model,
                'specifications': 1,
                'friendly_name': valve1.friendly_name,
                'ieee_address': valve1.ieee_address,
                'heat_required': valve1.heat_required,
                'measured_temperature': valve1.measured_temperature,
                'wanted_temperature': valve1.wanted_temperature,
                'schedule': schedule1.id,
                'schedule_name': schedule1.name,
        }

        self.assertEqual(expected, response.json())

    def test_update_temperature(self):

        authenticate(self.client)

        schedule1 = TemperatureSchedule.objects.create(
            name="Test schedule",
            schedule=generate_schedule_dict()
        )

        specification1 = ValveSpecifications.objects.create(
            manufacturer='Danfoss',
            model='014G2461',
            temperature_min=5,
            temperature_max=35,
            step=0.5
        )

        valve1 = HeaterValve.objects.create(
            manufacturer="Danfoss",
            model="014G2461",
            specifications=specification1,
            friendly_name="vanne_1",
            ieee_address="0xefzare233",
            heat_required=False,
            measured_temperature=23,
            wanted_temperature=30,
            schedule=schedule1,
        )

        response = self.client.get(self.url + f"{valve1.id}/")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['wanted_temperature'], 30)

        response = self.client.put(
            self.url + f"{valve1.id}/update_temperature/",
            {"wanted_temperature": 13},
            format='json'
        )

        self.assertEqual(response.status_code, 200)

    def test_update_schedule(self):

        authenticate(self.client)

        schedule1 = TemperatureSchedule.objects.create(
            name="Test schedule",
            schedule=generate_schedule_dict()
        )

        schedule2 = TemperatureSchedule.objects.create(
            name="New schedule",
            schedule=generate_schedule_dict()
        )

        specification1 = ValveSpecifications.objects.create(
            manufacturer='Danfoss',
            model='014G2461',
            temperature_min=5,
            temperature_max=35,
            step=0.5
        )

        valve1 = HeaterValve.objects.create(
            manufacturer="Danfoss",
            model="014G2461",
            specifications=specification1,
            friendly_name="vanne_1",
            ieee_address="0xefzare233",
            heat_required=False,
            measured_temperature=23,
            wanted_temperature=30,
            schedule=schedule1,
        )

        response = self.client.get(self.url + f"{valve1.id}/")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['schedule'], schedule1.id)
        self.assertEqual(response.json()['schedule_name'], schedule1.name)

        response = self.client.put(
            self.url + f"{valve1.id}/update_schedule/",
            {"schedule": schedule2.id},
            format='json'
        )

        self.assertEqual(response.status_code, 200)

        response = self.client.get(self.url + f"{valve1.id}/")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['schedule'], schedule2.id)
        self.assertEqual(response.json()['schedule_name'], schedule2.name)



class TestDefaultTemperature(APITestCase):
    """Test API endpoint for default temperatures"""

    url = reverse_lazy('defaulttemperatures-list')

    def test_list(self):

        authenticate(self.client)

        temperature1 = DefaultTemperature.objects.create(
            name="Comfort",
            letter_code='c',
            temperature=19
        )

        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)

        expected = [
            {
                'id': temperature1.id,
                'name': temperature1.name,
                'letter_code': temperature1.letter_code,
                'temperature': temperature1.temperature
            }
        ]

        self.assertEqual(response.json(), expected)

    def test_create(self):

        authenticate(self.client)

        temperature1_json = {"name": "Reduced", "letter_code": "r", "temperature": 17}

        response = self.client.post(self.url, temperature1_json, format='json')

        self.assertEqual(response.status_code, 201)

        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)

        expected = [
            {
                "id": 1,
                "name": "Reduced",
                "letter_code": "r",
                "temperature": 17
            }
        ]

        self.assertEqual(response.json(), expected)

    def test_delete(self):

        authenticate(self.client)

        temperature1 = DefaultTemperature.objects.create(
            name="Test temperature",
            letter_code='t',
            temperature=15
        )

        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)

        expected = [
            {
                "id": temperature1.id,
                "name": temperature1.name,
                "letter_code": temperature1.letter_code,
                "temperature": temperature1.temperature
            }
        ]

        self.assertEqual(response.json(), expected)

        response = self.client.delete(self.url + f"{temperature1.id}/")

        self.assertEqual(response.status_code, 204)

        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)

        expected = []

        self.assertEqual(response.json(), expected)

    def test_detail(self):

        authenticate(self.client)

        temperature1 = DefaultTemperature.objects.create(
            name="Test temperature",
            letter_code='t',
            temperature=15
        )

        response = self.client.get(self.url + f"{temperature1.id}/")

        self.assertEqual(response.status_code, 200)

        expected = {
            "id": temperature1.id,
            "name": temperature1.name,
            "letter_code": temperature1.letter_code,
            "temperature": temperature1.temperature
        }

        self.assertEqual(response.json(), expected)

    def test_update_temperature(self):

        authenticate(self.client)

        temperature1 = DefaultTemperature.objects.create(
            name="Test temperature",
            letter_code='t',
            temperature=15
        )

        response = self.client.put(
            self.url + f"{temperature1.id}/update_temperature/",
            {"temperature": 22}, format='json'
        )

        self.assertEqual(response.status_code, 200)

        response = self.client.get(self.url + f"{temperature1.id}/")

        self.assertEqual(response.status_code, 200)

        expected = {
            "id": temperature1.id,
            "name": "Test temperature",
            "letter_code": "t",
            "temperature": 22
        }

        self.assertEqual(response.json(), expected)


class TestSchedule(APITestCase):
    """Test API endpoint for schedules"""

    url = reverse_lazy('temperatureschedules-list')

    def test_list(self):

        authenticate(self.client)

        schedule1 = TemperatureSchedule.objects.create(
            name="Test schedule",
            schedule=generate_schedule_dict()
        )

        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)

        expected = [
            {
                "id": schedule1.id,
                "name": schedule1.name,
            }

        ]

        self.assertEqual(response.json(), expected)

    def test_create(self):

        authenticate(self.client)

        schedule1_json = {"name": "Created schedule", "schedule": generate_schedule_dict()}

        response = self.client.post(self.url, schedule1_json, format='json')

        self.assertEqual(response.status_code, 201)

        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)

        expected = [
            {
                "id": 1,
                "name": "Created schedule"
            }
        ]

        self.assertEqual(response.json(), expected)

    def test_delete(self):

        authenticate(self.client)

        schedule1 = TemperatureSchedule.objects.create(
            name="Test for deletion",
            schedule=generate_schedule_dict()
        )

        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)

        expected = [
            {
                "id": schedule1.id,
                "name": schedule1.name
            }
        ]

        self.assertEqual(response.json(), expected)

        response = self.client.delete(self.url + f"{schedule1.id}/")

        self.assertEqual(response.status_code, 204)

        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)

        expected = []

        self.assertEqual(response.json(), expected)

    def test_delete_protected(self):

        authenticate(self.client)

        schedule1 = TemperatureSchedule.objects.create(
            name="Test forbidden deletion",
            schedule=generate_schedule_dict()
        )

        valve1 = HeaterValve.objects.create(
            manufacturer="Danfoss",
            model="014G2461",
            specifications=None,
            friendly_name="vanne_1",
            ieee_address="0xefzare233",
            heat_required=False,
            measured_temperature=23,
            wanted_temperature=30,
            schedule=schedule1
        )

        with self.assertRaises(django.db.models.deletion.RestrictedError):
            response = self.client.delete(self.url + f"{schedule1.id}/")

    def test_detail(self):

        authenticate(self.client)

        schedule1 = TemperatureSchedule.objects.create(
            name="Schedule for detail",
            schedule=generate_schedule_dict()
        )

        response = self.client.get(self.url + f"{schedule1.id}/")

        self.assertEqual(response.status_code, 200)

        expected = {
            "id": schedule1.id,
            "name": schedule1.name,
            "schedule": generate_schedule_dict()
        }

        self.assertEqual(response.json(), expected)

    def test_update_schedule(self):

        authenticate(self.client)

        schedule1 = TemperatureSchedule.objects.create(
            name="Empty schedule",
            schedule=generate_schedule_dict()
        )

        response = self.client.get(self.url + f"{schedule1.id}/")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['schedule'], generate_schedule_dict())

        new_schedule = schedule1.schedule
        new_schedule["2"]["15"]["45"] = 20

        response = self.client.put(
            self.url + f"{schedule1.id}/update_schedule/",
            {"schedule": new_schedule},
            format='json'
        )

        self.assertEqual(response.status_code, 200)

        response = self.client.get(self.url + f"{schedule1.id}/")

        self.assertEqual(response.status_code, 200)

        expected = {
            "id": schedule1.id,
            "name": schedule1.name,
            "schedule": new_schedule
        }

        self.assertEqual(response.json(), expected)

    def test_update_name(self):

        authenticate(self.client)

        schedule1 = TemperatureSchedule.objects.create(name="Name to update", schedule=generate_schedule_dict())

        response = self.client.get(self.url + f"{schedule1.id}/")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['name'], "Name to update")

        response = self.client.put(self.url + f"{schedule1.id}/update_name/", {"name": "New name"}, format='json')

        self.assertEqual(response.status_code, 200)

        response = self.client.get(self.url + f"{schedule1.id}/")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['name'], "New name")

