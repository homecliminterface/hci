from django.utils import timezone
import zoneinfo


def generate_schedule_dict():
    """Create dictionary to store schedule information. Each HeaterValve object stores an instance of it (JSON format).

    Entries, whose keys are 0 to 6, represent the days of the week (starting on Sunday).
    Each entry contains a dictionary with 24 entries (keys 0 to 23) representing hours of the day.
    Each of those new entry corresponds to a dictionary, where a key is a minute and the corresponding
    entry is the temperature to be set.

    # Example
    `
    valve = HeaterValve(...)

    valve.schedule[3][8][30] = 19  # On wednesday, at time 8:30', send the order 19°C to the valve.
    `
    """
    schedule = dict()
    schedule[0] = 10;
    return schedule


class TimezoneMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        timezone.activate(zoneinfo.ZoneInfo('Europe/Paris'))
        return self.get_response(request)



