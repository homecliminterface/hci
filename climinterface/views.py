from rest_framework.viewsets import GenericViewSet, ModelViewSet
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, CreateModelMixin, DestroyModelMixin
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status

from django.views.generic import TemplateView
from django.utils.decorators import method_decorator

from csp.decorators import csp_update

from climinterface.serializers import HeaterValveSerializer, HeaterValveSetTemperatureSerializer, \
    HeaterValveSetScheduleSerializer, TemperatureScheduleSerializer, DefaultTemperatureSerializer, \
    TemperatureScheduleDetailSerializer, TemperatureScheduleUpdateScheduleSerializer, \
    TemperatureScheduleUpdateNameSerializer, DefaultTemperatureUpdateTemperatureSerializer
from climinterface.models import HeaterValve, TemperatureSchedule, DefaultTemperature


class HeaterValveViewset(ListModelMixin, RetrieveModelMixin, GenericViewSet):

    serializer_class = HeaterValveSerializer

    def get_queryset(self):
        return HeaterValve.objects.all()

    @action(detail=True, methods=['put'], serializer_class=HeaterValveSetTemperatureSerializer)
    def update_temperature(self, request, pk=None):
        """Interface to set a new wanted_temperature"""

        device = self.get_object()
        serializer = HeaterValveSetTemperatureSerializer(data=request.data)

        if serializer.is_valid():
            device.set_wanted_temperature(serializer.validated_data['wanted_temperature'])
            return Response({'status': 'temperature_set'})
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=['put'], serializer_class=HeaterValveSetScheduleSerializer)
    def update_schedule(self, request, pk=None):
        """Interface to set a new schedule"""

        device = self.get_object()
        serializer = HeaterValveSetScheduleSerializer(data=request.data)

        if serializer.is_valid():
            device.schedule = serializer.validated_data['schedule']
            device.save()
            return Response({'status': 'schedule_set'})
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TemperatureScheduleViewset(
    ListModelMixin,
    RetrieveModelMixin,
    CreateModelMixin,
    DestroyModelMixin,
    GenericViewSet
):

    serializers = {
        'list': TemperatureScheduleSerializer,
        'retrieve': TemperatureScheduleDetailSerializer,
        'create': TemperatureScheduleDetailSerializer,
        'update_schedule': TemperatureScheduleUpdateScheduleSerializer,
        'update_name': TemperatureScheduleUpdateNameSerializer
    }

    def get_serializer_class(self):
        return self.serializers.get(self.action, self.serializers['list'])

    def get_queryset(self):
        return TemperatureSchedule.objects.all()

    @action(detail=True, methods=['put'])
    def update_schedule(self, request, pk=None):
        """Interface to update schedule"""

        temperature_schedule = self.get_object()
        serializer = TemperatureScheduleUpdateScheduleSerializer(data=request.data)

        if serializer.is_valid():
            temperature_schedule.schedule = serializer.validated_data['schedule']
            temperature_schedule.save()
            return Response({'status': 'schedule_updated'})
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=['put'])
    def update_name(self, request, pk=None):
        """Interface to update name"""

        temperature_schedule = self.get_object()
        serializer = TemperatureScheduleUpdateNameSerializer(data=request.data)

        if serializer.is_valid():
            temperature_schedule.name = serializer.validated_data['name']
            temperature_schedule.save()
            return Response({'status': 'name_updated'})
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DefaultTemperatureViewset(
    ListModelMixin,
    RetrieveModelMixin,
    CreateModelMixin,
    DestroyModelMixin,
    GenericViewSet
):

    serializer_class = DefaultTemperatureSerializer

    def get_queryset(self):
        return DefaultTemperature.objects.all()

    @action(detail=True, methods=['put'], serializer_class=DefaultTemperatureUpdateTemperatureSerializer)
    def update_temperature(self, request, pk=None):
        """Interface to update name"""

        default_temperature = self.get_object()
        serializer = DefaultTemperatureUpdateTemperatureSerializer(data=request.data)

        if serializer.is_valid():
            default_temperature.temperature = serializer.validated_data['temperature']
            default_temperature.save()
            return Response({'status': 'temperature_updated'})
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


### NOT OK ###
#@csp_update(STYLE_SRC=["https://fonts.googleapis.com"], SCRIPT_SRC_ELEM=["https://cdn.jsdelivr.net/npm/redoc@next/bundles/redoc.standalone.js"])
#class ReDocView(TemplateView):
#    template_name='climinterface/redoc.html'
