# Generated by Django 4.0.5 on 2022-07-05 15:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('climinterface', '0009_alter_heatervalve_schedule'),
    ]

    operations = [
        migrations.AlterField(
            model_name='heatervalve',
            name='schedule',
            field=models.JSONField(default={0: None, 1: None, 2: None, 3: None, 4: None, 5: None, 6: None}, verbose_name='Temperatures schedule'),
        ),
    ]
