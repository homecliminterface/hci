# Generated by Django 4.0.5 on 2022-07-26 09:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('climinterface', '0014_defaulttemperature'),
    ]

    operations = [
        migrations.AlterField(
            model_name='heatervalve',
            name='schedule',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.RESTRICT, to='climinterface.temperatureschedule'),
        ),
    ]
