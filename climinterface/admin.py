from django.contrib import admin

from .models import ValveSpecifications, HeaterValve, ZigbeeDeviceBaseData, TemperatureSchedule, DefaultTemperature

admin.site.register(ValveSpecifications)
admin.site.register(HeaterValve)
admin.site.register(ZigbeeDeviceBaseData)
admin.site.register(TemperatureSchedule)
admin.site.register(DefaultTemperature)
