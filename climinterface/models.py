import zoneinfo

from django.db import models
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.utils import timezone
import json
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
from time import sleep

from climinterface.helpers import generate_schedule_dict
from climinterface.config import TOPIC_FILTER, VALVE_BASENAME, BROKER_ADDRESS


# Models for heater valves and technical specifications


class HeaterValve(models.Model):
    """Class for heater valves, with basic parameters"""

    manufacturer = models.fields.CharField(max_length=64, verbose_name="Manufacturer")
    model = models.fields.CharField(max_length=64, verbose_name="Model")
    specifications = models.ForeignKey('ValveSpecifications', null=True, on_delete=models.SET_NULL)
    friendly_name = models.fields.CharField(max_length=128, verbose_name="Friendly name")
    ieee_address = models.fields.CharField(max_length=25, verbose_name="IEEE address", unique=True)
    heat_required = models.fields.BooleanField(verbose_name="Heat required", null=True)
    measured_temperature = models.fields.FloatField(verbose_name="Measured temperature", null=True)
    wanted_temperature = models.fields.FloatField(verbose_name="Wanted temperature", null=True)
    schedule = models.ForeignKey('TemperatureSchedule', null=True, on_delete=models.RESTRICT)

    @classmethod
    def update_from_message(cls, message):
        """Method called in apps.CliminterfaceConfig.ready() method, whenever a new message is received.

        The role of this method is to keep database up to date from information received through MQTT messages.
        """

        print(message.topic)

        if (not message.topic.startswith(TOPIC_FILTER[:-1] + VALVE_BASENAME)) \
                or message.topic.endswith(("set", "get", "availability")):
            print("    ...HeaterValve update_from_message() not concerned by this message")
            return

        friendly_name = message.topic.removeprefix(TOPIC_FILTER[:-1])
        print(f"...updating {friendly_name} valve data from this message")

        try:
            instance, created = cls.objects.get_or_create(
                friendly_name=friendly_name,
                defaults={
                    'manufacturer': ZigbeeDeviceBaseData.objects.get(friendly_name=friendly_name).manufacturer,
                    'model': ZigbeeDeviceBaseData.objects.get(friendly_name=friendly_name).model,
                    'ieee_address': ZigbeeDeviceBaseData.objects.get(friendly_name=friendly_name).ieee_address

                }
            )
            if instance.specifications is None:
                print("Searching ValveSpecification entry for this model of HeaterValve...")
                instance.set_specifications()
                if instance.specifications is None:
                    print("This model of HeaterValve is not supported (ValveSpecification entry not found).")
                    return
            instance.heat_required = message.payload[instance.specifications.field_heat_required]
            instance.measured_temperature = message.payload[instance.specifications.field_measured_temperature]
            instance.wanted_temperature = message.payload[instance.specifications.field_wanted_temperature]
            instance.save()
        except ObjectDoesNotExist:
            print("Device not registered : no corresponding object of type ZigbeeDeviceBaseData")
        except KeyError:
            print("Some of required fields are not available yet. Delaying fields update")

    @classmethod
    def set_temperatures_from_schedules(cls):
        """For any HeaterValve object, read schedule entry corresponding to current time if present and set valve
        wanted temperature."""

        now = timezone.localtime(timezone.now())
        minute_of_week = now.weekday()*24*60 + now.hour*60 + now.minute

        for valve in cls.objects.all():
            if minute_of_week in valve.schedule.schedule:
                temperature = valve.schedule.schedule[minute_of_week]
                if isinstance(temperature, (int, float)):
                    valve.set_wanted_temperature(temperature)
                else:
                    try:
                        preset_temperature = DefaultTemperature.objects.get(letter_code=temperature).temperature
                        valve.set_wanted_temperature(preset_temperature)
                    except ObjectDoesNotExist:
                        print(
                            "Letter code {} in schedule for valve {} is unrecognized.",
                            temperature,
                            valve
                        )


    def __str__(self):
        return f'{self.friendly_name}'

    def set_specifications(self):
        """Add the foreign key to the corresponding ValveSpecifications object if it exists, do nothing if it does not.

        A given ValveSpecifications object is considered corresponding to the HeaterValve object
        if both fields 'manufacturer' and 'model' match, not considering the case.
        """

        try:
            specifications = ValveSpecifications.objects.get(
                Q(manufacturer__iexact=self.manufacturer) & Q(model__iexact=self.model)
            )
            self.specifications = specifications
            self.save()
        except ObjectDoesNotExist:
            pass
        except MultipleObjectsReturned:
            raise Exception("Two specifications for valves share same manufacturer and model")

    def set_wanted_temperature(self, wanted_temperature):
        """Send a MQTT message to zigbee device concerned by this HeaterValve instance, to change wanted temperature.

        This method does not modify the database ; wanted_temperature field in database will be modified
        at next MQTT message received about state of the concerned zigbee device.
        """

        topic = TOPIC_FILTER[:-1] + self.friendly_name + "/set"
        payload = '{{"{}": {}}}'.format(self.specifications.field_wanted_temperature, wanted_temperature)

        message = MQTTMessage(topic, payload)
        message.publish()
        print(f"Message queued to update temperature of {self.friendly_name} to {wanted_temperature}°C.")


class ValveSpecifications(models.Model):
    """Specific parameters depending on the model of the valve"""

    manufacturer = models.fields.CharField(max_length=64, verbose_name="Manufacturer")
    model = models.fields.CharField(max_length=64, verbose_name="Model")
    temperature_min = models.fields.IntegerField(verbose_name="Minimum temperature")
    temperature_max = models.fields.IntegerField(verbose_name="Maximum temperature")
    step = models.fields.FloatField(verbose_name="Step")

    field_heat_required = models.fields.CharField(max_length=64)
    field_measured_temperature = models.fields.CharField(max_length=64)
    field_wanted_temperature = models.fields.CharField(max_length=64)

    def __str__(self):
        return f'{self.manufacturer} {self.model}'


class ZigbeeDeviceBaseData(models.Model):
    ieee_address = models.fields.CharField(max_length=25, unique=True)
    friendly_name = models.fields.CharField(max_length=64)
    manufacturer = models.fields.CharField(max_length=64, null=True)
    model = models.fields.CharField(max_length=64, null=True)

    @classmethod
    def update_from_message(cls, message):
        """Update zigbee device list and base data from MQTT /device message.

        This database permits to link friendly_name to ieee_address and technical fields such as
        manufacturer and model fields.
        """
        if message.topic.startswith(TOPIC_FILTER[:-1] + "bridge/devices"):
            print("    ...updating ZigbeeDeviceBaseData objects from this message information")
            for device in message.payload:
                defaults = {'friendly_name': device['friendly_name']}
                if 'definition' in device and device['definition'] is not None:
                    if 'vendor' in device['definition']:
                        defaults['manufacturer'] = device['definition']['vendor']
                    if 'model' in device['definition']:
                        defaults['model'] = device['definition']['model']
                cls.objects.get_or_create(
                    ieee_address=device['ieee_address'],
                    defaults=defaults
                )

    def __str__(self):
        return f'{self.friendly_name}'


class TemperatureSchedule(models.Model):

    name = models.fields.CharField(max_length=64, unique=True)
    schedule = models.JSONField(default=generate_schedule_dict)

    def __str__(self):
        return "%d: %s" % (self.id, self.name)


class DefaultTemperature(models.Model):

    name = models.fields.CharField(max_length=32, unique=True)
    letter_code = models.fields.CharField(max_length=1, unique=True)
    temperature = models.fields.FloatField()

    def __str__(self):
        return "%d: %s" % (self.id, self.name)

# MQTT classes


class MessageProcessing:
    """Class for running mqtt client and use with Django Q."""

    def __init__(self, client):
        self.client = client

    @classmethod
    def create_and_start_paho_mqtt_client(cls):
        sleep(10)
        cls(mqtt.Client()).start_mqtt_client()

    @classmethod
    def handle_outgoing_message(cls, message):
        """Publish all MQTTMessage instances presents inf the outbound_queue

        An issue in paho_mqtt needs to create a client to send messages, to not use paho.mqtt.publish
        """

        publish.single(topic=message.topic, payload=message.payload, hostname=BROKER_ADDRESS)
        print("Message published :", message.topic, message.payload)

    @classmethod
    def handle_incoming_message(cls, message):
        ZigbeeDeviceBaseData.update_from_message(message)
        HeaterValve.update_from_message(message)

    def start_mqtt_client(self):
        """Start mqtt client, connect to server, subscribe to topic, store received messages in queue."""

        def on_connect(client, userdata, flags, rc):
            print("Connected with result code " + str(rc))
            client.subscribe(TOPIC_FILTER)

        def on_message(client, userdata, message):
            topic = message.topic
            json_payload = json.loads(str(message.payload.decode("utf-8")))
            message = MQTTMessage(topic, json_payload)
            print("Message reçu :", message.topic, str(message.payload))
            self.handle_incoming_message(message)

        self.client.on_connect = on_connect
        self.client.on_message = on_message

        self.client.connect(BROKER_ADDRESS)
        self.client.loop_start()


class MQTTMessage:
    """Message structure with topic and payload fields"""

    def __init__(self, topic, payload):
        self.topic = topic
        self.payload = payload

    def __str__(self):
        return f'{self.topic} {self.payload}'

    def publish(self):
        print("Message publié :", self.topic, str(self.payload))
        MessageProcessing.handle_outgoing_message(self)
