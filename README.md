TO DO LIST
_____

* Install python 10 virtual environment and requirements from
requirements.txt
* Check and modify settings file for deployment
* Adjust settings in [climinterface/config.py](climinterface/config.py) file
* Do the first migration
* Execute 

        ./manage.py loaddata specifications defaults,

to load known valve specifications and create default schedules and 
temperatures.
* Create a superuser and add users
* Run **both** SERVER ('./manage.py runserver hostname:port', for debug)
**and** QCLUSTER ('./manage.py qcluster', for debug) with built-in
development server (for testing) or with an external server (for deployment).
